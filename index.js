const db = require('./db')
const Express = require('express')
var bodyParser = require('body-parser');
const app = Express()
const cors = require('cors')
app.use(bodyParser.json())
app.use(cors())

const PORT = 3000

app.get('/get-all', (request, response) => {
    const num_clients = (request.params.numClients !== undefined)?request.params.numClients:5
    let promises = []
    for (let i = 1; i <= num_clients; i++) {
        const promise = db.getData(i)
        promises.push(promise)
    }
    Promise.all(promises).then((data) => {
        data = data.map(item => item.Item)
        response.status(200).send(data)
    }).catch((err) => {
        console.log(err)
        response.sendStatus(500)
    })
})

app.post('/post', (request, response) => {
    const data = request.body
    if (!data) {
        response.sendStatus(400)
    } else {
        db.postToDynamo(data).then(() => {
            response.sendStatus(200)
        }).catch((err) => {
            console.log(err)
            response.sendStatus(500)
        })
    }
})

app.listen(PORT, () => console.log("Listening on port 3000"))