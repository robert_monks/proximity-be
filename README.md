# Proximity-BE

## Overview
This will house our Express BE for the Proximity Hack The Office 3.0

## Endpoints
* POST to /post route with data in json format. { id: 1, clientCount: 20 }
* Request GET on /get-all to retrieve data for clients with ID 1 through 5.

### How do I get set up? ###
***ADD CONTENT HERE ONCE WE GET SET UP***

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
***ADD CONTENT HERE ONCE WE GET SET UP***

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
***ADD CONTENT HERE ONCE WE GET SET UP***

* Repo owner or admin
* Other community or team contact