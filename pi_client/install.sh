#!/usr/bin/env bash

mkdir -p /opt/proximity
cp * /opt/proximity
cp proximity.service /etc/systemd/system
cp proximity-listener.service /etc/systemd/system
systemctl daemon-reload
systemctl enable proximity

# Install Packages
apt-get install --assume-yes python3
apt-get install --assume-yes aircrack-ng

# Python setup
pip3 install --user pipenv
PATH=$HOME/.local/bin:$PATH

cd /opt/proximity
pipenv install
