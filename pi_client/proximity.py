#!/usr/bin/env python3

import requests
import time
import os
import csv
from datetime import datetime, timedelta

# Node CONFIG - START
node_id = 1

# Node CONFIG - END


# Probing for data
while True:

    with open('dump-01.csv', newline='') as csvfile:
        logreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        client_flag = False
        client_count = 0
        for row in logreader:
            if (len(row) > 0):
                if (row[0] == "Station MAC"):
                    client_flag = True
                    continue
                if (client_flag == True):
                    client_ls = datetime.strptime(row[2], ' %Y-%m-%d %H:%M:%S')
                    if (datetime.now()-timedelta(seconds=30) <= client_ls):
                        client_count += 1


    # Sending payload
    cur_time = time.time()
    jsobj = {"id": node_id,
            "date": int(cur_time*1000), # Convert to Milliseconds
            "clientCount": client_count}
    print(jsobj)
    time.sleep(5)
    #r = requests.get('http://34.227.83.230:3000/post', json=jsobj)
