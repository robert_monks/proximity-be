const AWS = require('aws-sdk')
AWS.config.update({ region: 'us-east-1' });
const dynamoDB = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' })

const postToDynamo = async (data) => {
    const { id, clientCount } = data
    console.log(data)
    const params = {
        TableName: "Proximity",
        Key: { ID: id },
        UpdateExpression: "set #d = :d, #n = :n",
        ExpressionAttributeNames: {
            "#d": "DateMs",
            "#n": "clientCount"
        },
        ExpressionAttributeValues: {
            ":d": Date.now(),
            ":n": clientCount
        }
    };
    
    return dynamoDB.update(params).promise()
}

const getData = async (id) => {

    const params = {
        TableName: "Proximity",
        Key: {
            ID: id
        }
    }

    return dynamoDB.get(params).promise()
}

module.exports = {
    postToDynamo,
    getData
}